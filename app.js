var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var tasks = require('./routes/tasks');
var login = require('./routes/login');
var register = require('./routes/register');


var port = 4200;
var app = express();
//set up template engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

//static files
app.use(express.static(path.join(__dirname, 'client')));

//my body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/',index);
app.use('/api',tasks);
app.use('/login',login);
app.use('/register',register);

//listen to port
app.listen(port);
console.log('listening to port '+ port);