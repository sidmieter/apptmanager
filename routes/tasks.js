var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db =mongojs('mongodb://test:test@ds161262.mlab.com:61262/appdb',['tasks']);


//get ALL tasks
router.get('/tasks',function(req,res,next) {
	db.tasks.find(function(err, tasks){
		if(err){
			res.send(err);

		}
		res.json(tasks);
	});
	//res.render('index.html');
});

//get single tasks
router.get('/task/:id',function(req,res,next) {
	db.tasks.findOne({_id: mongojs.ObjectId(req.params.id)}, function(err, task){
		if(err){
			res.send(err);

		}
		res.json(task);
	});
	//res.render('index.html');
});

//save task
router.post('/task', function(req, res, next){
	var task = req.body;
	if(!task.title || (task.isDone + '')){
		res.status(400);
		res.json({
			"error": "Bad Data"
		});

	} else {
		db.tasks.save(task, function(err, task){
			if(err){
			res.send(err);

		}
			res.json(task);
		});
	}
});

//delete task
router.delete('/task/:id',function(req,res,next) {
	db.tasks.remove({_id: mongojs.ObjectId(req.params.id)}, function(err, task){
		if(err){

			res.send(err);
		}
		res.json(task);
	});
	//res.render('index.html');
});


//update
router.put('/task/:id',function(req,res,next) {
	var task = req.body;
	var updTask = {};

	if (task.isDone) {
		updTask.isDone = task.isDone;
	}
	if (task.title) {
		updTask.title = task.isDone;
	}
	if(!updTask){
		
		res.status(400);
		res.json({
			"error": "Bad Data"
		});
	}  else {db.tasks.remove({_id: mongojs.ObjectId(req.params.id)}, updTask, {}, function(err, tasks){
		if(err){
			res.send(err);

		}
		res.json(task);
	});
}
});
	
module.exports = router;
